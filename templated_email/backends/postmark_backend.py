import vanilla_django
from django.conf import settings
from django.utils.translation import ugettext as _
import postmark
from . import PostmarkMissingAPIException
import logging


class TemplateBackend(vanilla_django.TemplateBackend):
    """
    Backend which uses Django's
    templates, and django-postmark send email function.

    Heavily inspired by http://stackoverflow.com/questions/2809547/creating-email-templates-with-django

    Default / preferred behaviour works like so:
        templates named
            templated_email/<template_name>.email

        {% block subject %} declares the subject
        {% block plain %} declares text/plain
        {% block html %} declares text/html

    Legacy behaviour loads from:
        text/plain part:
            templated_email/<template_name>.txt
        text/html part:
            templated_email/<template_name>.html

        Subjects for email templates can be configured in one of two ways:

        * If you are using internationalisation, you can simply create entries for
          "<template_name> email subject" as a msgid in your PO file

        * Using a dictionary in settings.py, TEMPLATED_EMAIL_DJANGO_SUBJECTS,
          for e.g.:
          TEMPLATED_EMAIL_DJANGO_SUBJECTS = {
            'welcome':'Welcome to my website',
          }

    Subjects are templatable using the context, i.e. A subject
    that resolves to 'Welcome to my website, %(username)s', requires that
    the context passed in to the send() method contains 'username' as one
    of it's keys

    You must add django-postmark settings to your settings.py. See https://github.com/themartorana/python-postmark/#readme for details.

    Add a tag for your email by using named parameter 'tag'

    Does NOT support attachments, yet...
    """

    def __init__(self, *args, **kwargs):
        vanilla_django.TemplateBackend.__init__(self, *args, **kwargs)

    def send(self, template_name, from_email, recipient_list, context, cc=None,
            bcc=None, fail_silently=False, headers=None, template_prefix=None,
            template_suffix=None, template_dir=None, file_extension=None,
            **kwargs):

        postmark_api_key = getattr(settings, 'POSTMARK_API_KEY', None)
        postmark_sender = getattr(settings, 'POSTMARK_SENDER', None)

        # Check for postmark api key (required)
        if not postmark_api_key:
            if not fail_silently:
                raise PostmarkMissingAPIException('You must add django-postmark settings to your settings.py. See https://github.com/themartorana/python-postmark/#readme for details.')
            else:
                logging.error('You must add django-postmark settings to your settings.py. See https://github.com/themartorana/python-postmark/#readme for details.')
                return

        # get template parts to use in email body
        parts = self._render_email(template_name, context,
                                   template_dir=template_prefix or template_dir,
                                   file_extension=template_suffix or file_extension)

        # check from email (required either as default from settings or passed in to function)
        from_email = from_email or postmark_sender
        if not from_email:
            if not fail_silently:
                raise PostmarkMissingAPIException('No sender email provided. Either provide to send function or add POSTMARK_SENDER to your settings.py.')
            else:
                logging.error('No sender email provided. Either provide to send function or add POSTMARK_SENDER to your settings.py.')
                return

        # check for tag
        tag = getattr(kwargs, 'tag', None)

        message = postmark.PMMail(
                sender = from_email,
                subject = "[Massey CMS] Article Deployment Error",
                to = recipient_list,
                cc = cc,
                bcc = bcc,
                custom_headers=headers or {},
                tag=tag,
                text_body = parts.get('plain', ''),
                html_body = parts.get('html', ''))

        message.send()